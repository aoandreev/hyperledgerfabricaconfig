#!/bin/bash

echo
echo " ____    _____      _      ____    _____ "
echo "/ ___|  |_   _|    / \    |  _ \  |_   _|"
echo "\___ \    | |     / _ \   | |_) |   | |  "
echo " ___) |   | |    / ___ \  |  _ <    | |  "
echo "|____/    |_|   /_/   \_\ |_| \_\   |_|  "
echo
echo "Build your first network (BYFN) end-to-end test"
echo
MCHANNEL_NAME="$1"
YCHANNEL_NAME="$2"
DELAY="$3"
LANGUAGE="$4"
TIMEOUT="$5"
VERBOSE="$6"
MART_FOLDER="$7"
YART_FOLDER="$8"
: ${MCHANNEL_NAME:="mychannel"}
: ${YCHANNEL_NAME:="yourchannel"}
: ${DELAY:="3"}
: ${LANGUAGE:="golang"}
: ${TIMEOUT:="10"}
: ${VERBOSE:="false"}
: ${MART_FOLDER:="mchannel-artifacts"}
: ${YART_FOLDER:="ychannel-artifacts"}
LANGUAGE=`echo "$LANGUAGE" | tr [:upper:] [:lower:]`
COUNTER=1
MAX_RETRY=5

CC_SRC_PATH="github.com/chaincode/chaincode_example02/go/"
if [ "$LANGUAGE" = "node" ]; then
	CC_SRC_PATH="/opt/gopath/src/github.com/chaincode/chaincode_example02/node/"
fi

echo "Channel name : "$MCHANNEL_NAME

# import utils
. scripts/utils.sh

createChannelM() {
	setGlobals 0 1

	if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
                set -x
		peer channel create -o orderer.example.com:7050 -c $MCHANNEL_NAME -f ./${MART_FOLDER}/channel.tx >&log.txt
		res=$?
                set +x
	else
				set -x
		peer channel create -o orderer.example.com:7050 -c $MCHANNEL_NAME -f ./${MART_FOLDER}/channel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
		res=$?
				set +x
	fi
	cat log.txt
	verifyResult $res "MChannel creation failed"
	echo "===================== MChannel '$MCHANNEL_NAME' created ===================== "
	echo
}

echo "Channel name : "$YCHANNEL_NAME

createChannelY() {
	setGlobals 0 3

	if [ -z "$CORE_PEER_TLS_ENABLED" -o "$CORE_PEER_TLS_ENABLED" = "false" ]; then
                set -x
		peer channel create -o orderer.example.com:7050 -c $YCHANNEL_NAME -f ./${YART_FOLDER}/channel.tx >&log.txt
		res=$?
                set +x
	else
				set -x
		peer channel create -o orderer.example.com:7050 -c $YCHANNEL_NAME -f ./${YART_FOLDER}/channel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
		res=$?
				set +x
	fi
	cat log.txt
	verifyResult $res "YChannel creation failed"
	echo "===================== YChannel '$YCHANNEL_NAME' created ===================== "
	echo
}

joinChannelM () { 
	for org in {1..3}; do #there must be all organizations
	    for peer in 0 1; do
		joinChannelWithRetry $MCHANNEL_NAME $peer $org
		echo "===================== peer${peer}.org${org} joined channel '$MCHANNEL_NAME' ===================== "
		sleep $DELAY
		echo
	    done
	done
}

joinChannelY () { 
	for org in {3..5}; do #there must be all organizations
	    for peer in 0 1; do
		joinChannelWithRetry $YCHANNEL_NAME $peer $org
		echo "===================== peer${peer}.org${org} joined channel '$YCHANNEL_NAME' ===================== "
		sleep $DELAY
		echo
	    done
	done
}


##############################################################################################
## Create channel
# echo "Creating channel mychannel..."
# createChannelM

echo "Creating channel yourchannel..."
createChannelY

##############################################################################################
## Join all the peers to the channel
# echo "Having 1-3 peers join the mychannel..."
# joinChannelM

echo "Having 3-5 peers join the yourchannel..."
joinChannelY

##############################################################################################
## Set the anchor peers for each org in the channel
# echo "Updating anchor peers for org1..."
# updateAnchorPeers $MCHANNEL_NAME 0 1 $MART_FOLDER
# echo "Updating anchor peers for org2..."
# updateAnchorPeers $MCHANNEL_NAME 0 2 $MART_FOLDER
echo "Updating anchor peers for org3..."
updateAnchorPeers $YCHANNEL_NAME 0 3 $YART_FOLDER

echo "Updating anchor peers for org4..."
updateAnchorPeers $YCHANNEL_NAME 0 4 $YART_FOLDER
echo "Updating anchor peers for org5..."
updateAnchorPeers $YCHANNEL_NAME 0 5 $YART_FOLDER

##############################################################################################
## Install chaincode on peer0.org1 and peer0.org2
# echo "Installing chaincode on peer0.org1..."
# installChaincode 0 1
# echo "Install chaincode on peer0.org2..."
# installChaincode 0 2
echo "Install chaincode on peer0.org3..."
installChaincode 0 3
echo "Install chaincode on peer0.org4..."
installChaincode 0 4
echo "Install chaincode on peer0.org5..."
installChaincode 0 5
############################################################################################################################
# echo "Instantiating chaincode on peer0.org2..."
# instantiateChaincode $MCHANNEL_NAME 0 2

# # Query chaincode on peer0.org1
# echo "Querying chaincode on peer0.org1..."
# chaincodeQuery $MCHANNEL_NAME 0 1 100

# # Invoke chaincode on peer0.org1 and peer0.org2
# echo "Sending invoke transaction on peer0.org1 peer0.org2..."
# chaincodeInvoke $MCHANNEL_NAME 0 1 0 2 0 3

# # Query chaincode on peer0.org1
# echo "Querying chaincode on peer0.org1..."
# chaincodeQuery $MCHANNEL_NAME 0 1 90
# echo "Querying chaincode on peer0.org2..."
# chaincodeQuery $MCHANNEL_NAME 0 2 90
# echo "Querying chaincode on peer0.org3..."
# chaincodeQuery $MCHANNEL_NAME 0 3 90

############################################################################################################################
echo "Instantiating chaincode on peer0.org4..."
instantiateChaincode $YCHANNEL_NAME 0 4

# Query chaincode on peer0.org1
echo "Querying chaincode on peer0.org1..."
chaincodeQuery $YCHANNEL_NAME 0 4 100

# Invoke chaincode on peer0.org1 and peer0.org2
echo "Sending invoke transaction on peer0.org1 peer0.org2..."
chaincodeInvoke $YCHANNEL_NAME 0 3 0 4 0 5

# Query chaincode on peer0.org1
echo "Querying chaincode on peer0.org3..."
chaincodeQuery $YCHANNEL_NAME 0 3 100
echo "Querying chaincode on peer0.org4..."
chaincodeQuery $YCHANNEL_NAME 0 4 100
echo "Querying chaincode on peer0.org5..."
chaincodeQuery $YCHANNEL_NAME 0 5 100


echo
echo "========= All GOOD, BYFN execution completed =========== "
echo

echo
echo " _____   _   _   ____   "
echo "| ____| | \ | | |  _ \  "
echo "|  _|   |  \| | | | | | "
echo "| |___  | |\  | | |_| | "
echo "|_____| |_| \_| |____/  "
echo

exit 0
